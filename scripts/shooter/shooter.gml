function spawn_bullet(spawn_direction, adjust)
{
  var d = global.d;
  var x_adjust = 0;
  var x_spd = 0;
  var y_adjust = 0;
  var y_spd = 0;
  
  switch (spawn_direction)
  {
    case d.r:
      x_adjust = adjust;
      x_spd = spd;
      break;
    case d.u:
      y_adjust = adjust;
      y_spd = spd * -1;
      break;
    case d.l:
      x_adjust = adjust;
      x_spd = spd * -1;
      break;
    case d.d:
      y_adjust = adjust;
      y_spd = spd;
      break;    
  }
  var blt = instance_create_layer(x+x_adjust, y+y_adjust, "Instances", o_bullet);
  blt.direction = direction;
  blt.dmg = dmg;
  blt.x_spd = x_spd;
  blt.y_spd = y_spd;
}

function shoot()
{
  var d = global.d;
  switch(direction)
  {
  case d.r:
    spawn_bullet(d.r, half_w);
    break;
  case d.u:
    spawn_bullet(d.u, half_h * -1);
    break;
  case d.l:
    spawn_bullet(d.l, half_w * -1);
    break;
  case d.d:
    spawn_bullet(d.d, half_h);
    break;
  }
}