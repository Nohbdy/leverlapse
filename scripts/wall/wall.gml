function walls_disappear(_color, _player) {
  var current_wall;
  switch (_color)
  {
    case c.red:
      for (var i = 0; i < ds_list_size(_player.red_walls); i++)
      {
        current_wall = ds_list_find_value(_player.red_walls, i);
        if (instance_exists(current_wall))
        {
          current_wall.visible = !current_wall.visible;
        }
      }
      draw_self();
      break;
    case c.green:
      for (var i = 0; i < ds_list_size(_player.green_walls); i++)
      {
        current_wall = ds_list_find_value(_player.green_walls, i);
        if (instance_exists(current_wall))
        {
          current_wall.visible = !current_wall.visible;
        }
      }
      draw_self();
      break;
    case c.blue:
      for (var i = 0; i < ds_list_size(_player.blue_walls); i++)
      {
        current_wall = ds_list_find_value(_player.blue_walls, i);
        if (instance_exists(current_wall)) { current_wall.visible = !current_wall.visible; }
      }
      draw_self();
      break;
  }
}