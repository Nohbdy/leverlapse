game_settings =
{
  kb_mode : "toggle"
  // chg = change - as in functions to change settings
  chg :
  {
    // Toggles kb_mode between toggle and hold
    kb_mode : function() constructor
    {
      if (game_settings.kb_mode == "toggle")
      {
        kb_mode = "hold";
      }
      else
      {
        kb_mode = "toggle";
      }
    }
  }
}